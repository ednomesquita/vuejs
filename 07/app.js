Vue.component('task-list', {
    template: `
    <div><task v-for="task in tasks" :task="task"> {{ task.task }}</task></div>
    
        `,
    data() {
        return {
            tasks: [
                {task: 'Go to the store', complete: false},
                {task: 'Go to the store', complete: false}
            ]
        }
    }        
});



Vue.component('task', {
    template: `<li><slot></slot></li>`
})

const app = new Vue({
    el: '#app'
});



