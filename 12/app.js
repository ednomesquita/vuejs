Vue.component('coupon', {
  template: `
    <input placeholder="Enter your coupon" @blur="$emit('applied')"> 
  
  `,

  // methods: {
  //   onCouponApplied(){
  //     this.$emit('applied')
  //   }

  // }
})


const app = new Vue({
    el: '#app',
    data: {
        isApplied: false
    },

    methods: {
      onCouponApplied() {
        this.isApplied = true
      }
    }
});
