// window.Event = new class {
//   constructor() {
//     this.vue = new Vue();
//   }


//   fire(event, data = null) {
//     this.vue.$emit(event, data )
//   }

//   listen(event, callback) {
//     this.vue.$on(event, callback)
//   }
// }

window.Event = new Vue()

Vue.component('coupon', {
  template: `
    <input placeholder="Enter your coupon" @blur="onCouponApplied"> 
  
  `,

  methods: {
    onCouponApplied(){
      // this.$emit('applied')
      // Event.fire('applied')

      Event.$emit('applied')
    }

  }
})


const app = new Vue({
    el: '#app', 
    data: {
        isApplied: false
    },

    // methods: {
    //   onCouponApplied() {
    //     Event.listen('applied', () => alert('aaaaaaaaaaaaaaaaaaaa'))
    //     // this.isApplied = true
    //   }
    // }

    created() {
      // Event.listen('applied', () => alert('aaaaaaaaaaaaaaaaaaaa'))
      Event.$on('applied', () => alert('bbbbbbbbbbbbbbbb'))
    }
});
